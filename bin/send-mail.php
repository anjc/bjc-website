<?php
/**
* Author: Luis Zuno
* Email: luis@luiszuno.com
* URL: http://www.luiszuno.com
* Version: 1.0.0 
**/

//vars
$subject = $_POST['subject'];
$to = explode(',', $_POST['to'] );

$from = $_POST['email'];

//data
$msg = "NAME: "  .$_POST['name']    ."<br>\n";
$msg .= "EMAIL: "  .$_POST['email']    ."<br>\n";
$msg .= "COMMENTS:<br>"  .$_POST['comments']    ."<br>\n";

//Headers
$headers  = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=UTF-8\r\n";
$headers .= "From: ".$_POST['name']." <".$from. ">\r\n";
$headers .= "Reply-To: ".$from."\r\n";
//$headers .= "Return-Path: ".$from."\r\n";
$headers .= "X-Mailer: PHP/".phpversion();


//send for each mail
foreach($to as $mail){
   mail($mail, $subject, $msg, $headers);
}

?>
