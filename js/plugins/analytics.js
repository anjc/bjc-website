(function analytics() {
	'use strict';

	var _paq = _paq || [];
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function () {
		var u = "//monits.karolak.fr/stats/",
			d = document,
			g = d.createElement('script'),
			s = d.getElementsByTagName('script')[0];
		_paq.push(['setTrackerUrl', u + 'piwik.php']);
		_paq.push(['setSiteId', 3]);
		g.type = 'text/javascript';
		g.async = true;
		g.defer = true;
		g.src = u + 'piwik.js';
		s.parentNode.insertBefore(g, s);
	}());
}());
