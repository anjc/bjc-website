/**
 * Created by Victor on 15/12/2014.
 */

jQuery(document).ready(function() {

    if (window.matchMedia("(min-width: 460px)").matches) {
        var aide = '<li><div class="menu-aide"><a id="aide-selected" href="#" class="test" data-i18n="Aide" onclick="javascript:startTrip();">Aide</a></div></li>';

        $('.navbar-nav').append(aide);
        $(aide).animate("shake");
    }
})

function startTrip() {
    var trip1 = new Trip([
            {
                sel: $("#main-add-button"),
                content: "<h5> Ajouter une fenêtre</h5> <p> Ce bouton permet d'ouvrir une fenêtre à droite de la fenêtre active.</p>",
                position: "w",
                expose: true,
                onTripStart: function (tripIndex, tripObject) {
                    adapteCss();
                }
            },
            {
                sel: $("#main-config-button"),
                content: "<h4> Paramétres </h4><p>Permet de modifier les paramétres de lecture </p>",
                position: "w",
                expose: true,
                onTripStart: function (tripIndex, tripObject) {
                    adapteCss();
                }
            },
            {
                sel: $("#main-fullscreen-button"),
                content: "<br><h4> Mode grand écran </h4> <p>Afficher la bible en mode plein écran.</p>",
                position: "w",
                expose: true,
                onTripStart: function (tripIndex, tripObject) {
                    adapteCss();
                }
            },
            {
                sel: $("#menu-book"),
                content: "<h4>Choisir un livre</h4><p> Cliquer dans cette zone pour faire apparaitre la liste des livres puis les chapitres.</p>",
                position: "e",
                expose: true
            },
            {
                sel: $("#menu-version-book"),
                content: "<h4>Choisir la version de la bible</h4> <p>Utiliser cette liste pour choisir une version de la bible.</p>",
                position: "e",
                expose: true
            },
            {
                sel: $("#menu-info"),
                content: "<h4>Informations</h4> <p>Informations sur la version de la bible selectionnée",
                position: "e",
                expose: true
            }],
        {
            showNavigation: true,
            showCloseBox: true,
            delay: -1
        });

    if (window.matchMedia("(min-width: 460px)").matches) {
        trip1.start();
    }

    function subtour() {
        var trip2 = new Trip([
                {
                    sel: $("#main-add-button"),
                    content: "<br><h4> Ajouter une fenêtre</h4> <p> Ce bouton permet d'ouvrir une fenêtre à droite de la fenêtre active.</p>",
                    position: "w",
                    expose: true
                },
                {
                    sel: $("#main-config-button"),
                    content: "<br><h4> Paramétres </h4> <p> Permet de modifier les paramétres de lecture </p>",
                    position: "w",
                    expose: true
                },
                {
                    sel: $("#main-fullscreen-button"),
                    content: "<br><h4> Mode grand écran </h4> <p>Afficher la bible en mode plein écran.</p>",
                    position: "w",
                    expose: true
                },
                {
                    sel: $("#menu-book"),
                    content: "<h4>Choisir un livre</h4><p> Cliquer dans cette zone pour faire apparaitre la liste des livres puis les chapitres.</p>",
                    position: "e",
                    expose: true
                },
                {
                    sel: $("#menu-version-book"),
                    content: "<h4>Choisir la version de la bible</h4> <p>Utiliser cette liste pour choisir une version de la bible.</p>",
                    position: "e",
                    expose: true
                },
                {
                    sel: $("#menu-info"),
                    content: "<h4>Informations</h4> <p>Informations sur la version de la bible selectionnée",
                    position: "e",
                    expose: true
                }],
            {
                showNavigation: true,
                showCloseBox: true,
                delay: -1
            });

        // Start new Tour
        //trip2.start();
    }

    /**
     function moveHighlight(element){
        var top, left = 0;
        var parents = $(element).parents();

        for (var i = 0; i < parents.length; i++) {
            console.log(parents[i]);
            var top =  top + $(parents[i]).position().top;
            var left =  left + $(parents[i]).position().left;
        }

        console.log(" top" + top + " left " + left );
        console.log( $(element).width());
        console.log( $(element).height());

        var id = $(element).position();

        $("#trip-show").offset({
            "top": top,
            "left": id.left
        });

        $("#trip-show").css({
            "width": $(element).width(),
            "height": $(element).height(),
            "float": $(element).css("float"),
            "margin": $(element).css("margin"),
            "padding": $(element).css("padding")
        });

        //console.log($("#trip-show"));

    }
     **/

    function adapteCss(){

        // $('.trip-block.animated.tada').addClass('bock-show');
        $('.trip-block.w:before').removeClass('trip-show');

        $('.tada').css('margin-top','35px');
        $('.trip-block.w:before').css({
            'top': '20px'
        });
    }


    function resetCss(){

        $('.tada').removeClass('block-show');
        $('.trip-block.w:before').removeClass('trip-show');

        /**
        $('.tada').css('margin-top','35px');
        $('.trip-block.w:before').css({
            'top': '20px'
        });
         **/
    }


}