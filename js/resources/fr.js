sofia.resources['fr'] = {
	"translation": {
		"name": "Français",
		"menu": {
			"search": {
				"placeholder": "Recherche"
			},
			"config": {
				"font": "Style",
				"settings": "Paramètres",
				"tools": "Outils"
			},
			"reset": "réinitialiser",
			"themes": {
				"default": "Normal",
				"sepia": "Sépia",
				"dark": "Mode Nuit"
			},
			"bjc": {
				"home": "Accueil",
				"bible": "Bible",
                "download": "Télécharger",
                "order": "Commander",
                "donate": "Soutenir",
				"contact": "Contact"
			}
		},
		"plugins": {
			"visualfilters": {
				"button": "Filtres visuels",
				"title": "Filtres visuels",
				"newfilter": "Nouveau filtre",
				"strongsnumber": "De Strong #",
				"morphology": "Morphologie",
				"style": "Style"
			},
			"eng2p": {
				"button": "Anglais 2ème personne pluriel",
				"title": "Anglais Deuxième personne pluriel"
			},
			"lemmapopup": {
				"findalloccurrences": "Trouver toutes les occurrences (environ __count__)"
			}
		},
		"windows": {
			"bible": {
				"label": "Bible",
				"filter": "Filtrer...",
				"ot": "Tanakh",
				"nt": "Le Testament de Yéhoshoua",
				"dc": "Deutérocanoniques",
				"more": "Plus",
				"less": "Moins",
				"recentlyused": "Récemment utilisés",
				"languages": "Langues",
				"countries": "Pays"
			},
			"commentary": {
				"label": "Commentaire"
			},
			"map": {
				"label": "Cartes",
				"placeholder": "Recherche ..."
			},
			"search": {
				"label": "Recherche",
				"placeholder": "Recherche",
				"button": "Recherche",
				"results": "Résultats",
				"verses": "versets",
				"options": "Options de recherche"
			},
			"media": {
				"label": "Médias"
			},
			"dictionary": {
                                "label": "Dictionnaire"
            },
			"gralphabet": {
                                "label": "Alphabet Grec"
            },
			"hebalphabet": {
                                "label": "Alphabet Hébraïque"
            },
			"notes": {
				"label": "Remarques"
			},
			"audio": {
				"options": "Options audio",
				"synctext": "Synchronisation du texte (beta)",
				"autoplay": "Autoplay Suivant",
				"drama": "Drame",
				"nondrama": "Non Drame"
			},
			"comparison": {
				"label": "Comparaison",
				"button": "Comparer"
			},
			"tooltips": {
				"textnav": "Référence",
				"textlist": "Version",
				"info": "Information Version",
				"audio": "Ecouter"
			}
		},
		"names": {
			"en": "French"
		},
		"window": {
			"bible": {
				"languages": "indéfini"
			}
		}
	}
}
