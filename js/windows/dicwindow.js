
var DictionaryWindow = function(id, node, init_data) {



	var
		container =
			$('<div class="dictionaries-container">'+
				'<div class="window-header dictionaries-header"><span class="window-title i18n" data-i18n="[html]windows.dictionary.label"></span></div>'+
				'<div class="container-fluid window-main dictionaries-main">' +
				'</div>' +
			'</div>').appendTo(node),

		// dom nodes
		header = container.find('.dictionaries-header'),
		main = container.find('.window-main'),

		// settings
		hasFocus = false;
		
		$('.dictionaries-main').append(
			'<div class="row tab other-tab">'+
				'<div class="section" data-textid="fra_bjc">'+
					'<table id="dic_table" class="table table-striped table-bordered">'+
						'<thead>'+
							'<tr>'+
								'<th>Terme</th>'+
								'<th>Etymologie</th>'+
								'<th>Définition</th>'+
							'</tr>'+
						'</thead>'+
						'<tfoot>'+
							'<tr>'+
								'<th>Terme</th>'+
								'<th>Etymologie</th>'+
								'<th>Définition</th>'+
							'</tr>'+
						'</tfoot>'+
					'</table>'+
				'</div>'+
			'</div>'
		);


	// START UP

	function init() {

		// console.log('dicoindow init',init_data);

		if (init_data == null) {
			return;
		}

		var table = $('#dic_table').DataTable( {
			"ajax": "content/dictionaries/dictionnaire.json",
			"columns": [
				{ "data": "word" },
				{ "data": "etymology" },
				{ "data": "definition" }
			],
			 "language": {
				"lengthMenu": "Afficher _MENU_ enregistrements par page",
				"zeroRecords": "Aucun résultat trouvé",
				"info": "Affichage de la page _PAGE_ sur _PAGES_",
				"infoEmpty": "Aucun enregistrement disponible",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"next": "Suivant",
					"previous": "Précédent"
					},
				"search": "Recherche"
			},
			"pageLength": 25,
			"autoWidth": false,
			"responsive": true,
			"lengthChange": false
		 } );
 
		table.on( 'draw', function () {
			var body = $( table.table().body() );
	 
			body.unhighlight();
			if ( table.rows( { filter: 'applied' } ).data().length ) {
				body.highlight( table.search() );
			}
		} );
	}

	// function startup() {


		// if (textsInitialized && parallelsData != null) {
			// load !
			// loadParallelData();
		// }
	// }

	init();


	function close() {

		ext.clearListeners();
	}

	function size(width, height) {
		// do notheirng?
		main.outerHeight(height - header.outerHeight())
			.outerWidth(width);

	}

	var ext = {
		size: size,
		getData: function() {
			return {

				params: {
					'win': 'dictionary'
				}

			}
		},
		close: close
	};
	ext = $.extend(true, ext, EventEmitter)

	return ext;
};

sofia.initMethods.push(function() {

	sofia.windowTypes.push( {
		className:'DictionaryWindow',
		param: 'dictionary',
		paramKeys: {
			'textid': 't',
			'parallelid': 'p'
		},
		init: {
			'textid': sofia.config.newBibleWindowVersion
		}
	});

});
